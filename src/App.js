import React, { useState } from 'react'
import './App.css'
import Sidebar from './components/sidebar/Sidebar'
import Chat from './components/chat/Chat'
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"
import Login from './components/login/Login'
import { useStateValue } from './conf/StateProvider'

function App() {
  
  const [{ user }, dispatch] = useStateValue()

  return (
    <div className="app">
      {!user ? (
        <Login />
      ): (
        <div class="app__body">
          <Sidebar/>
            <Switch>
          
              <Route path="/rooms/:roomId">
                <Chat />
              </Route>

              <Route path="/">
                <Chat />
              </Route>

            </Switch>
        </div>
      )}
    </div>
  );
}

export default App;
