import React from 'react'
import './Login.css'    
import { Button } from '@material-ui/core'
import { auth, provider } from '../../conf/firebase'
import { useStateValue } from '../../conf/StateProvider'
import { actionTypes } from '../../conf/Reducer'

function Login() {
    const [{}, dispatch] = useStateValue()
    const siginIn = () => {
        auth.signInWithPopup(provider)
        .then(
            (result) => {
                dispatch({
                    type: actionTypes.SET_USER,
                    user: result.user,
                })
            }
        ).catch(
            (error) => {
                alert(error.message)
            }
        )
    }
    return (
        <div className='login'>
            <div class="login__container">
                <img src="https://upload.wikimedia.org/wikipedia/commons/6/6b/WhatsApp.svg"
                 alt="" />
                 <div class="login__text">
                     <h1>Sigin in to WhatsApp</h1>
                 </div>
                <Button onClick={siginIn}>
                    Sigin in with Google
                </Button>
            </div>
        </div>
    )
}

export default Login
