import React, { useEffect, useState } from 'react'
import './Sidebar.css'
import { Avatar, IconButton } from '@material-ui/core'
import DonutLargeIcon from '@material-ui/icons/DonutLarge'
import ChatIcon from '@material-ui/icons/Chat'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import SearchOutLined from '@material-ui/icons/SearchOutlined'
import SidebarChat from './SidebarChat'
import db from '../../conf/firebase'
import { useStateValue } from '../../conf/StateProvider'

function Sidebar() {

    const [rooms, setRooms] = useState([])
    const [{ user }, dispatch] = useStateValue()

    useEffect(() => {
        const unsuscribe = db.collection('rooms').onSnapshot(
            (snapshot) => setRooms(
                snapshot.docs.map((doc) => ({
                    id: doc.id,
                    data: doc.data()
                }))
            )
        )
        return () => {
            unsuscribe()
        } 
    }, [])

    return (
        <div className="sidebar">
            <div class="sidebar__header">
                <Avatar src={user?.photoURL}/>
                <div class="sidebar__headerRight">
                    <IconButton>
                        <DonutLargeIcon />
                    </IconButton>
                    <IconButton>
                        <ChatIcon />
                    </IconButton>
                    <IconButton>
                        <MoreVertIcon />
                    </IconButton>
                </div>                
            </div>
            <div class="sidebar__search">
                <div class="sidebar__searchContainer">
                    <SearchOutLined />
                    <input placeholder="Search or start new chat" type="text" />
                </div>
            </div>
            <div class="sidebar__chats">
                <SidebarChat addNewChat/>
                {
                    rooms.map(
                        (room) => (
                            <SidebarChat key={room.id} id={room.id} name={room.data.name} />
                        )
                    )
                }
            </div>
        </div>
    )
}

export default Sidebar
